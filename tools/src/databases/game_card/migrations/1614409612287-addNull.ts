import {MigrationInterface, QueryRunner} from "typeorm";

export class addNull1614409612287 implements MigrationInterface {
    name = 'addNull1614409612287'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "game_history"."created_at" IS NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ALTER COLUMN "created_at" SET DEFAULT 'now()'`);
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "opened_cards"`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "opened_cards" text`);
        await queryRunner.query(`COMMENT ON COLUMN "game_card"."created_at" IS NULL`);
        await queryRunner.query(`ALTER TABLE "game_card" ALTER COLUMN "created_at" SET DEFAULT 'now()'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "game_card" ALTER COLUMN "created_at" SET DEFAULT '2021-02-27 03:37:20.865262+00'`);
        await queryRunner.query(`COMMENT ON COLUMN "game_card"."created_at" IS NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "opened_cards"`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "opened_cards" character varying(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ALTER COLUMN "created_at" SET DEFAULT '2021-02-27 03:37:20.865262+00'`);
        await queryRunner.query(`COMMENT ON COLUMN "game_history"."created_at" IS NULL`);
    }

}
