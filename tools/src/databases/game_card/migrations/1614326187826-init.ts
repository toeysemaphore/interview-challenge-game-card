import {MigrationInterface, QueryRunner} from "typeorm";

export class init1614326187826 implements MigrationInterface {
    name = 'init1614326187826'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "game_history" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT 'now()', "updated_at" TIMESTAMP WITH TIME ZONE, "is_active" boolean NOT NULL DEFAULT false, "id" uuid NOT NULL, "openCard" integer NOT NULL, "previousOpenCard" integer NOT NULL, "openedCards" character varying(255) NOT NULL, "game_card_id" uuid, CONSTRAINT "PK_0e74b90c56b815ed54e90a29f1a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "game_card" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT 'now()', "updated_at" TIMESTAMP WITH TIME ZONE, "is_active" boolean NOT NULL DEFAULT false, "id" uuid NOT NULL, "username" character varying(100) NOT NULL, "clicktimes" integer NOT NULL, CONSTRAINT "PK_b3a1cbc416f2533b795c147eee2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD CONSTRAINT "FK_b9cdb540a970d5dbf1590364028" FOREIGN KEY ("game_card_id") REFERENCES "game_card"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "game_history" DROP CONSTRAINT "FK_b9cdb540a970d5dbf1590364028"`);
        await queryRunner.query(`DROP TABLE "game_card"`);
        await queryRunner.query(`DROP TABLE "game_history"`);
    }

}
