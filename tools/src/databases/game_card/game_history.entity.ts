import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from "typeorm";
import { BaseEntity } from "../core/base_entity";
import { EGameCard } from "./game_card.entity";

@Entity("game_history")
export class EGameHistory extends BaseEntity {
  @PrimaryColumn({
    type: "uuid",
    name: "id",
  })
  id: string;

  @Column({ type: "integer", name: "open_card_index" })
  openCardIndex: number;

  @Column({ type: "integer", name: "open_card_value" })
  openCardValue: number;

  @Column({ type: "integer", name: "previous_open_card_index" })
  previousOpenCardIndex: number;

  @Column({ type: "integer", name: "previous_open_card_value" })
  previousOpenCardValue: number;

  @Column({
    type: "text",
    name: "opened_cards",
    nullable: true,
  })
  openedCards: string;

  @ManyToOne((_) => EGameCard, (e) => e.histories)
  @JoinColumn({
    name: "game_card_id",
  })
  gameCard: EGameCard;
}
