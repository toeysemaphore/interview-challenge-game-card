import React, { useState } from "react";
import { useRouter } from "next/router";

const Home: React.FunctionComponent = () => {
  const router = useRouter();
  const [username, setUsername] = useState<string>("");

  function handleOnSubmit() {
    if (username == "") {
      alert("please fill form");
      return;
    }
    // only english and numeric allowed
    const valid = /^[A-Za-z][A-Za-z0-9]*$/.test(username);
    if (!valid) {
      alert("only english and numeric allowed");
      setUsername("");
      return;
    }
    localStorage.setItem("username", username);
    router.push("/game-card");
  }

  function handleOnchangeUsername({ target: { value } }: any) {
    setUsername(value);
  }

  return (
    <>
      <div className="home_container">
        <div className="home_container__top_decorate"></div>
        <div className="home_container__bottom_decorate"></div>
        <div className="user_name_container">
          <input onChange={handleOnchangeUsername} placeholder="username" />
          <button id="submit-username" onClick={handleOnSubmit}>Submit</button>
        </div>
      </div>
    </>
  );
};

export default Home;
