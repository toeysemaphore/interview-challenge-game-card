import React from "react";
import { CardServiceImpl } from "../services/card_service";
import { delay } from "../utils/time_dalay";

const _defaultCards = () =>
  Array.from(new Array(12).keys()).map((index) => ({
    index,
    value: null,
  }));

function findIndexOfTemp(temps: any[], index: number) {
  return temps.find((temp) => temp.index === index);
}

const usePlayCard = (callback: Function): any => {
  const [cards, setCards] = React.useState(_defaultCards());

  const [tempCards, setTempCards] = React.useState<any>([]);

  const [round, setRound] = React.useState(0);

  const service = new CardServiceImpl();

  function resetGame() {
    setCards(_defaultCards());
    setTempCards([]);
    localStorage.removeItem("game_id");
    setRound(0);
  }

  async function tapCard(cardIndex: number) {
    const username = localStorage.getItem("username");
    const gameID = localStorage.getItem("game_id");

    if (!username || !gameID) {
      alert("something went wrong not found username or game id");
      return;
    }
    try {
      if (tempCards.length == 2) {
        return;
      }
      const previousCard = tempCards.length == 0 ? -1 : tempCards[0].index;

      const played = await service.playGame(
        username as string,
        gameID as string,
        cardIndex,
        previousCard
      );

      setRound(played.round as number);

      let temps = [
        ...tempCards,
        {
          index: cardIndex,
          value: played.currentCardValue,
        },
      ];

      setTempCards(temps);

      setCards(
        // open card
        cards.map((card) => {
          if (temps.length == 0) {
            card.value = null;
            return card;
          }
          const temp = findIndexOfTemp(temps, card.index);
          if (temp) {
            card.value = temp.value;
          }
          return card;
        })
      );

      setTimeout(() => {
        if (temps.length == 2) {
          if (temps[0].value != temps[1].value) {
            alert("mismatch");
            setCards(
              cards.map((card) => {
                const open = played?.openedCards.find(
                  (c: any) => c.cardIndex == card.index
                );
                if (!open) {
                  card.value = null;
                }
                return card;
              })
            );
          }
          setTempCards([]);
        }
      }, 1000);

      if (played.openedCards.length == 12) {
        await delay(1200);
        // game done callback
        if (callback) callback();
      }
    } catch (error) {
      console.log("ERRORR", error);
    }
  }

  return [cards, round, tapCard, resetGame];
};

export default usePlayCard;
