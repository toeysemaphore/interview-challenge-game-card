import React from "react";
import { CardServiceImpl } from "../services/card_service";

// avoid interval won't stop
// using global variable
let globalInterval: any = null;

// this is polling strategy you can replace with websocket for better performace (reduce tcp handshake)
const useGlobalScore = (every: number = 1000) => {
  // save old state
  let oldScore = 0;
  const [score, setScore] = React.useState(0);
  const [isNewScore, setIsNewScore] = React.useState(false);
  const service = new CardServiceImpl();

  async function updateScore() {
    try {
      const result: number = (await service.getGlobalScore()) as number;
      // re render only when score changed
      if (result != oldScore) {
        setScore(result);
        oldScore = result;
        setIsNewScore(true);
      } else {
        setIsNewScore(false);
      }
    } catch (error) {
      clearInterval(globalInterval);
      if (!confirm("something went wrong cannot fetch data.\n Retry ?")) {
        window.location.href = "/";
      }
    }
  }

  // clear interval
  React.useEffect(() => {
    globalInterval = setInterval(() => {
      updateScore();
    }, every);

    return () => {
      // dispose polling
      if (globalInterval) clearInterval(globalInterval);
    };
  }, []);

  return [score, isNewScore] as any;
};

export default useGlobalScore;
