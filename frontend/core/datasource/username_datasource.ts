/**
 * @deprecated
 */
export abstract class UsernameDatasource {
  abstract getUsername(): string | null;
  abstract setUsername(username: string): void;
  abstract removeUsername(): void;
}

/**
 * @deprecated
 */
export class UsernameDatasourceImpl implements UsernameDatasource {
  constructor(private readonly localStorate: Storage) {}
  removeUsername(): void {
    return this.localStorate.removeItem("game_id");
  }
  getUsername(): string | null {
    return this.localStorate.getItem("username");
  }
  setUsername(username: string): void {
    return this.localStorate.setItem("username", username);
  }
}
