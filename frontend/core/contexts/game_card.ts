import { createContext } from "react";
import { CardService } from "../services/card_service";

/**
 * @deprecated
 */
const defaultState: {
  username: string;
  cardService: CardService | null;
  gameID: string | null;
  updateGameID: any;
  updateUsername: any;
} = {
  username: "",
  cardService: null,
  gameID: "",
  updateGameID: null,
  updateUsername: null,
};

/**
 * @deprecated
 */
const context = createContext(defaultState);

export default context;
