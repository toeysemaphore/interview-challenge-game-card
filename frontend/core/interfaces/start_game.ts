export interface IStartGameResponse {
  username: string;
  gameID: string;
}
