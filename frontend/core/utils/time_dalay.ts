export const delay = (time: number) =>
  new Promise((res) => setTimeout(() => res(null), time));
