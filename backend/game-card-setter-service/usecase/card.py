import json


class CardUsecase:
    def __init__(self, mq, db, redis):
        self._mq = mq
        self._db = db
        self._redis = redis

    def _new_game_callback(self, ch, method, properties, body):
        data = json.loads(body.decode())
        print("RECEIVED [_new_game_callback] ", data)
        cur = self._db.cursor()

        cards = ",".join(str(index) for index in data['cards'])

        cur.execute(
            "INSERT INTO game_card (id,username,clicktimes,cards,created_at) VALUES (%s, %s, %s,%s,'now()')",
            (data['game_id'], data['username'], 0, cards))

        self._db.commit()

    def new_game_consume(self):
        self._mq.basic_consume(queue='new_game',
                               auto_ack=True,
                               on_message_callback=self._new_game_callback)

    def _update_user_score_callback(self, ch, method, properties, body):
        data = json.loads(body.decode())
        print("RECEIVED [_update_user_score_callback] ", data)
        username = data["username"]
        cur = self._db.cursor()
        cur.execute(
            "SELECT clicktimes FROM game_card WHERE username = %s  and clicktimes > 11 ORDER BY 1 ASC LIMIT 1",
            [username],
        )
        score = cur.fetchone()

        self._redis.set("USER_BEST_SCORE-{}".format(username), score[0])

    def update_user_score_consume(self):
        self._mq.basic_consume(
            queue='update_user_score',
            auto_ack=True,
            on_message_callback=self._update_user_score_callback)

    def _game_finish_callback(self, ch, method, properties, body):
        data = json.loads(body.decode())
        print("RECEIVED [_game_finish_callback] ", data)
        game_id = data["game_id"]
        cur = self._db.cursor()
        cur.execute(
            "UPDATE game_card  SET is_active = true where id = %s",
            [game_id],
        )
        self._db.commit()

    def game_finish_consume(self):
        self._mq.basic_consume(queue='game_finish',
                               auto_ack=True,
                               on_message_callback=self._game_finish_callback)

    def _play_game_callback(self, ch, method, properties, body):
        print("RECEIVED [_play_game_callback] ", body.decode())
        data = json.loads(body.decode())
        cur = self._db.cursor()
        cur.execute(
            """
                    INSERT INTO game_history 
                    (id,game_card_id,open_card_index,open_card_value,previous_open_card_index,previous_open_card_value,opened_cards,created_at)
                     VALUES (%s, %s, %s,%s, %s, %s,%s,'now()')
                    """,
            (data['id'], data['game_id'], data['current_card_index'],
             data['current_card_value'], data['previous_card_index'],
             data['previous_card_value'], json.dumps(data['opened_cards'])))

        cur.execute(
            """
                    UPDATE game_card 
                    SET clicktimes = %s , updated_at = 'now()'
                    WHERE id = %s;
                    """, (data["round"], data["game_id"]))
        self._db.commit()

    def play_game_consume(self):
        self._mq.basic_consume(queue='play_game',
                               auto_ack=True,
                               on_message_callback=self._play_game_callback)
