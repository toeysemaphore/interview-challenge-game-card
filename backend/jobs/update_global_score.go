package main

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	_ "github.com/lib/pq"
)

func main() {
	portStr := os.Getenv(("DATABASE_PORT"))

	port, err := strconv.Atoi(portStr)

	if err != nil {
		panic(err)
	}

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		os.Getenv("DATABASE_HOST"),
		port,
		os.Getenv("DATABASE_USERNAME"),
		os.Getenv("DATABASE_PASSWORD"),
		"game_card",
	)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	defer db.Close()

	err = db.Ping()

	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully database is connected!")

	rdb := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")),
		Password: "", // no password set
	})

	if err := rdb.Ping(context.TODO()).Err(); err != nil {
		panic(err)
	}

	defer rdb.Close()

	row := db.QueryRow("SELECT clicktimes FROM game_card where clicktimes > 11 and is_active = true ORDER BY 1 LIMIT 1")

	if err := row.Err(); err != nil {
		panic(err)
	}

	var score int

	err = row.Scan(&score)

	if err != nil {
		panic(err)
	}

	if err := rdb.Set(context.TODO(), "GLOBAL_SCORE", strconv.Itoa(score), time.Hour).Err(); err != nil {
		panic(err)
	}

	fmt.Println("update global score success !!!")

}
