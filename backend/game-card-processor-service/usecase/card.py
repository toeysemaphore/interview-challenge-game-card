from random import shuffle
from uuid import uuid4
import json
import os


class CardUsecase:
    def __init__(self, redis, postgres, mq, cards):
        self._redis = redis
        self._postgres = postgres
        self._mq = mq
        self._cards = cards

    def change_mq(self, mq):
        print("recevied channel mq closed try to reconnect")
        self._mq = mq

    def _generateCards(self):
        # copy by value
        temp_cards = self._cards[:]
        shuffle(temp_cards)
        return temp_cards

    def _publish_to_queue(self, queue, data):
        try:
            self._mq.basic_publish(exchange='', routing_key=queue, body=data)
        except:
            # force close
            # no time to build rabbitmq self healing
            # let k8s manage instead
            os._exit(1)

    def _build_game_key(self, game_id):
        return "GAME-{}".format(game_id)

    def _build_game_history_key(self, game_id):
        return "GAME-HISTORY-{}".format(game_id)

    def _validate_card_index(self, index):
        if index == -1:
            return
        if index < 0 or index > 11:
            raise Exception("wrong card index")

    def get_global_score(self):
        score = self._redis.get("GLOBAL_SCORE")

        if score == None:
            return 0

        return int(score.decode())

    def _validate_opened_card(self, opened_cards, card_index):
        for open_card in opened_cards:
            if open_card["card_index"] == card_index:
                raise Exception("this card is already opened")

    def play_game(self, game_id, username, current_card_index,
                  previous_card_index):

        self._validate_card_index(current_card_index)
        self._validate_card_index(previous_card_index)

        redis_game_key = self._build_game_key(game_id)

        result_byte = self._redis.get(redis_game_key)

        if result_byte == None:
            raise Exception("Not Found")

        gameData = json.loads(result_byte.decode())

        if gameData["username"] != username:
            raise Exception("Forbidden")

        if gameData["is_finish"]:
            raise Exception("Game is over")

        cards = gameData["cards"]

        history_result_byte = self._redis.get(
            self._build_game_history_key(game_id))
        new_game_history = None

        is_card_matched = True

        history_result = []

        current_card_value = cards[current_card_index]

        # play first time
        if history_result_byte == None:
            is_card_matched = False
            new_game_history = {
                "current_card_index": current_card_index,
                "current_card_value": current_card_value,
                "previous_card_index": -1,
                "previous_card_value": -1,
                "opened_cards": []
            }

        else:

            history_result: list = json.loads(history_result_byte.decode())

            # last history
            opened_cards: list = history_result[len(history_result) -
                                                1]["opened_cards"]

            self._validate_opened_card(opened_cards, current_card_index)
            self._validate_opened_card(opened_cards, previous_card_index)

            previous_card_value = -1

            if previous_card_index != -1:
                previous_card_value = cards[previous_card_index]

                # matched card
                if previous_card_value == current_card_value:
                    opened_cards.append({
                        "card_index": current_card_index,
                        "card_value": current_card_value
                    })
                    opened_cards.append({
                        "card_index": previous_card_index,
                        "card_value": previous_card_value
                    })
                else:
                    is_card_matched = False
            new_game_history = {
                "current_card_index": current_card_index,
                "current_card_value": current_card_value,
                "previous_card_index": previous_card_index,
                "previous_card_value": previous_card_value,
                "opened_cards": opened_cards
            }

        history_result.append(new_game_history)

        # BAD CODE !!!
        # I have to time to clean it
        new_game_history["id"] = str(uuid4())
        new_game_history["game_id"] = game_id
        new_game_history["round"] = len(history_result)
        # BAD CODE

        history_result_json = json.dumps(history_result)

        self._redis.set(self._build_game_history_key(game_id),
                        history_result_json)

        self._publish_to_queue("play_game", json.dumps(new_game_history))

        opened = history_result[len(history_result) - 1]["opened_cards"]

        # finish game
        if len(opened) == 12:
            gameData["is_finish"] = True
            self._redis.set(redis_game_key, json.dumps(gameData))
            self._publish_to_queue("game_finish",
                                   json.dumps({"game_id": game_id}))
            # update best score
            self._publish_to_queue("update_user_score",
                                   json.dumps({"username": username}))

        return {
            "is_card_matched": is_card_matched,
            "round": len(history_result),
            "current_card_value": current_card_value,
            "opened_cards": opened
        }

    def get_user_best_score(self, username):
        score = self._redis.get("USER_BEST_SCORE-{}".format(username))

        if score == None:
            return -1

        return int(score.decode())

    def start_game(self, username):

        game_id = str(uuid4())

        redis_game_key = self._build_game_key(game_id)

        cards = self._generateCards()

        data = json.dumps({
            "username": username,
            "game_id": game_id,
            "cards": cards,
            "is_finish": False
        })

        self._redis.set(redis_game_key, data)

        self._publish_to_queue("new_game", data)

        return game_id
