import redis
import threading
from usecase.card import CardUsecase
import json
import pika
from fastapi import FastAPI
import os
from fastapi.middleware.cors import CORSMiddleware
from dto.request_body import PlayGameRequest,StartNewGameRequest

cards = [1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6]


def declare_queues(channel, queues):
    for v in queues:
        channel.queue_declare(queue=v)


    
amqp = pika.BlockingConnection(pika.ConnectionParameters(os.environ["RABBITMQ_HOST"]))

channel = amqp.channel()

declare_queues(channel, ["new_game", "play_game","update_user_score","game_finish","liveness"])

r = redis.Redis(host=os.environ["REDIS_HOST"], port=6379, password='')


usecase = CardUsecase(r, None, channel, cards)


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post("/start_game")
def start_game(body: StartNewGameRequest):
    game_id = usecase.start_game(body.username)
    return {"username": body.username, "game_id": game_id}

@app.get("/score/{username}/best")
def get_username_score(username: str):
    score = usecase.get_user_best_score(username)
    return {
        "score": score
    }

@app.get("/score")
def global_score():
    score = usecase.get_global_score()
    return {
        "score": score
    }
    

@app.post("/play_game/{game_id}")
def start_game(game_id: str, body: PlayGameRequest):
    result = usecase.play_game(game_id, body.username, body.current_card_index,
                               body.previous_card_index)
    return result


def _liveness_callback(self, ch, method, properties, body):
    print("LIVENESS.....")
# avoid channel closed

def isolate_thread_liveness_consume():
    channel.basic_consume(
            queue='liveness',
            auto_ack=True,
            on_message_callback=_liveness_callback)
threading.Thread(target=isolate_thread_liveness_consume)

print(' [*] Waiting for messages. To exit press CTRL+C')

channel.start_consuming()