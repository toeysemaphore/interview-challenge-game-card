import json
from dto.game import GameHistoryDto, OpenCardDto


# deprecated
class CardRepository:
    def __init__(self, redis, mq, postgres):
        self._redis = redis
        self._mq = mq
        self._postgres = postgres

    def _build_game_key(self, game_id: str) -> str:
        return "GAME-{}".format(game_id)

    def _build_game_history_key(self, game_id: str) -> str:
        return "GAME-HISTORY-{}".format(game_id)

    def saveGameHistory(self, game_id: str, value: str):
        key = self._build_game_history_key(game_id)

    def findGameHistory(self, game_id: str): GameHistoryDto
        key = self._build_game_history_key(game_id)
        history_result_byte = self._redis.get(key)

        if history_result_byte == None:
            return None

        history_result_json = json.loads(history_result_byte.decode())
        history = GameHistoryDto(
            history_result_json["current_card_index"],
            history_result_json["current_card_value"],
            history_result_json['previous_card_index'],
            history_result_json["previous_card_value"], [
                OpenCardDto(c["card_index"], c["card_value"])
                for c in history_result_json["opened_cards"]
            ])
            
        return history

    def _publish_to_queue(self, queue: str, data: str):
        self._mq.basic_publish(exchange='', routing_key=queue, body=data)