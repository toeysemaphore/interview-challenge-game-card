import json


class OpenCardDto:
    card_index: int
    card_value: int

    def __init__(self, card_index: int, card_value: int):
        self.card_index = card_index
        self.card_value = card_value

    def to_json(self):
        return json.dumps({
            "card_index": self.card_index,
            "card_value": self.card_value
        })


class GameHistoryDto:
    current_card_index: int
    current_card_value: int
    previous_card_index: int
    previous_card_value: int
    opened_cards: list[OpenCardDto]

    def __init__(
        self,
        current_card_index: int,
        current_card_value: int,
        previous_card_index: int,
        previous_card_value: int,
        opened_cards: list[OpenCardDto],
    ):
        self.current_card_index = current_card_index
        self.current_card_value = current_card_value
        self.previous_card_index = previous_card_index
        self.previous_card_value = previous_card_value
        self.opened_cards = opened_cards

    def to_json(self):
        return json.dumps({
            "current_card_index":
            self.current_card_index,
            "current_card_value":
            self.current_card_value,
            "previous_card_index":
            self.previous_card_index,
            "previous_card_value":
            self.previous_card_value,
            "opened_cards": [card.to_json() for card in self.opened_cards],
        })
